require('./ajax');
var btnMenu = document.getElementById("btn-menu");
var mainMenu = document.getElementById("main-menu");
btnMenu.addEventListener("click", function() {
    if (mainMenu.classList.contains('active')) {
        mainMenu.classList.remove('active');
    } else {
        mainMenu.classList.add('active');
    }
});
window.moveTo = function(id) {
    to = document.getElementById(id);
    to.scrollIntoView({
        block: "start",
        behavior: "smooth"
    });
}
window.sendForm = function(e) {
    e.preventDefault();
    nombre = e.target.nombre.value;
    startup = e.target.startup.value;
    pais = e.target.pais.value;
    web_app = e.target.web_app.value;
    telefono = e.target.telefono.value;
    mail = e.target.mail.value;
    mensaje = e.target.mensaje.value;
    btn_submit = document.getElementById("submit_btn");
    btn_submit.setAttribute("disabled", "true");
    ajax({
        data: {
            nombre: nombre,
            startup: startup,
            pais: pais,
            web_app: web_app,
            telefono: telefono,
            mail: mail,
            mensaje: mensaje
        },
        success: function() {
            data = JSON.parse(response);
            if (data.status == 'enviado') {
                info = document.getElementById('form-info');
                info.classList.remove("d-none");
                setTimeout(function() {
                    info.classList.add("d-none");
                }, 5000);
            } else {
                info = document.getElementById('form-danger');
                info.classList.remove("d-none");
                setTimeout(function() {
                    info.classList.add("d-none");
                }, 5000);
            }
            btn_submit.removeAttribute("disabled");
        }
    });
}