window.ajax = function(data){
	xhr = getAjax();
	xhr.open('POST','/sendMail',true);
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4){
			response = xhr.responseText;
			data.success(response);
		}
	}
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
	xhr.send("data="+JSON.stringify(data.data));
}

function getAjax(){
	xmlhttp = false;
	try{
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}catch(e){
		try{
			xmlhttp = new ActiveXObject("MsXML2.XMLHTTP");
		}catch(E){
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest != 'undefined'){
		xmlhttp = new XMLHttpRequest();
	}

	return xmlhttp;
}