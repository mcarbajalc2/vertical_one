<!DOCTYPE html>
<html lang="es" class="h-100">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Vertical One</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body>
	<header id="header">
		<div class="d-flex flex-wrap flex-md-nowrap">
			<div class="container d-flex top">
				<a href="/" class="ml-0 logo">
					<img src="{{ asset('images/logo.png') }}">
				</a>
				<button id="btn-menu" class="btn-menu mr-0 ml-auto d-md-none">
					<label></label>
					<label></label>
					<label></label>
				</button>
			</div>
			<nav id="main-menu" class="mr-0 ml-auto d-md-block col-12 col-md-auto p-0">
				<ul class="d-flex flex-column flex-md-row list-unstyled m-0 h-100">
					<li class=""><a class="d-flex align-items-center h-100" onclick="moveTo('que_aportamos')">¿QUÉ APORTAMOS?</a></li>
					<li class=""><a class="d-flex align-items-center h-100" onclick="moveTo('clientes')">¿CONSEGUIMOS CLIENTES?</a></li>
					<li class=""><a class="d-flex align-items-center h-100" onclick="moveTo('buscamos')">¿A QUIÉNES BUSCAMOS?</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<section class="banner">
		<div class="container d-flex flex-column justify-content-center align-items-center h-100">
			<div class="text-center caption">
				<h1 class="mb-3">LIDERANDO EL CRECIMIENTO DE NUEVOS NEGOCIOS</h1>
				<p class="mb-3">VERTICAL ONE es una organización dedicada a buscar emprendimientos<wbr> con alto potencial de crecimiento y acelerar este proceso para convertirlas en líderes en su categoría.</p>
				<a class="btn btn-primary" onclick="moveTo('contact_form')">Contactar</a>
			</div>
		</div>
	</section>
	<section class="que_aportamos" id="que_aportamos">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					<img src="{{ asset('images/que_aportamos.png') }}" class="w-100">
				</div>
				<div class="col-12 col-md-6 d-flex flex-column justify-content-center">
					<h3 class="mb-3">¿Qué aportamos?</h3>
					<p class="pr-1">En VERTICAL ONE nos encargamos de que tu base de clientes crezca, para que tú puedas enfocarte en tu oferta y servicio.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="conseguimos_clientes" id="clientes">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					<h3 class="mb-3">¿Esto quiere decir que te conseguimos clientes?</h3>
					<p class="mb-4">Sí, y en grandes volúmenes. Autilizando diferentes técnicas basadas en el análisis inteligente de datos, ayudamos a que cualquier start-up alcance un crecimiento sostenible de manera rápida y segura.</p>
					<p>Además, a lo largo del proceso te asesoramos para mejorar tu producto.</p>
				</div>
			</div>
		</div>		
	</section>
	<section class="quienes_buscamos" id="buscamos">
		<div class="container">
			<div class="row mb-3">
				<div class="col-12 text-center">
					<h3 class="mb-3">¿A quiénes buscamos?</h3>
					<p class="mc2-p-1">Para ser parte del programa LEADGROWTH tienes que pasar por una evaluación. Buscamos empresas que tengan las siguientes características:</p>
				</div>				
			</div>
			<div class="row mb-5">
				<div class="col-12 col-md-6">
					<img src="{{ asset('images/a_quienes_buscamos.png') }}" class="w-100">
				</div>
				<div class="col-12 col-md-6 d-flex flex-column justify-content-center">
					<ul class="list-unstyled caracteristicas">
						<li><i class="far fa-check-circle mr-2"></i>Un modelo de negocio diferenciado con una propuesta de valor clara.</li>
						<li><i class="far fa-check-circle mr-2"></i>Capacidad de escalar rápidamente en tiempos cortos.</li>
						<li><i class="far fa-check-circle mr-2"></i>Un equipo enfocado en brindar servicios de primera</li>
						<li><i class="far fa-check-circle mr-2"></i>Un producto en el mercado.</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-12 mb-3">
					<p>Sabemos que cada start-up tiene diferentes necesidadas y recursos, y nuestros modelos contemplan diferentes fórmulas de asociación que se adaptan a cada empresa. ¡El objetivo es que crezcas!</p>
					<p>Para ser parte de LEADGROWTH debes llenar el siguiente formulario y uno de nuestros especialistas en BUSINESS DEVELOPMENT se pondrá en contacto contigo:</p>
				</div>
			</div>
			<div class="alert alert-success d-none" role="alert" id="form-info">
			  Formulario enviado.
			</div>
			<div class="alert alert-danger d-none" role="alert" id="form-danger">
			  Error al enviar formulario.
			</div>
			<form class="row" onsubmit="sendForm(event)" id="contact_form">
				<div class="col-12 col-md-6 d-flex flex-column">
					<input type="text" placeholder="Nombre" name="nombre" required>
					<input type="text" placeholder="Startup" name="startup" required>
					<input type="text" placeholder="País" name="pais" required>
					<input type="tel" placeholder="Teléfono" name="telefono" required>
				</div>
				<div class="col-12 col-md-6 d-flex flex-column mb-3">
					<input type="email" placeholder="E-mail" name="mail" required>
					<input type="text" name="web_app" placeholder="Web/App">
					<textarea name="mensaje" placeholder="¿Qué más nos puedes contar de ustedes?" rows="3" required></textarea>
				</div>
				<div class="col-12 text-center">
					<button type="submit" class="btn btn-primary" id="submit_btn">Enviar</button>
				</div>
			</form>
		</div>
	</section>
	<footer id="footer" class="d-flex pt-3">
		<div class="container text-center">
			<p><small>Todos los derechos reservados, VerticalOne &copy; 2018.</small></p>
		</div>
	</footer>
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>