
<h3>Información del visitante</h3>

<div>
	<p><b>Nombre:</b>&nbsp;{{ $demo->nombre }}</p>	
	<p><b>Startup:</b>&nbsp;{{ $demo->startup }}</p>
	<p><b>País:</b>&nbsp;{{ $demo->pais }}</p>
	<p><b>E-mail:</b>&nbsp;{{ $demo->mail }}</p>
	<p><b>Teléfono:</b>&nbsp;{{ $demo->telefono }}</p>
	<p><b>Web/App:</b>&nbsp;{{ $demo->web_app }}</p>
	<p><b>Mensaje:</b>&nbsp;{{ $demo->mensaje }}</p>
</div>