<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    //
    public function send(Request $request){
    	$data = json_decode($request->input('data'));

    	$nombre = $data->nombre;
    	$startup = $data->startup;
    	$pais = $data->pais;
    	$web_app = $data->web_app;
        $telefono = $data->telefono;
        $mail = $data->mail;
    	$mensaje = $data->mensaje;

        $objDemo = new \stdClass();
        $objDemo->startup = $startup;
        $objDemo->pais = $pais;
        $objDemo->nombre = $nombre;
        $objDemo->web_app = $web_app;
        $objDemo->telefono = $telefono;
        $objDemo->mail = $mail;
        $objDemo->mensaje = $mensaje;
        $objDemo->subject = 'Web Form Mail';

        try{
            Mail::to("grafilab@gmail.com")->send(new DemoEmail($objDemo));
            $status = ['status'=>'enviado'];
        }catch(Exception $e){
            $status = ['status'=>'error'];
        }
    	
    	return $status;
    }
}
