/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(3);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
var btnMenu = document.getElementById("btn-menu");
var mainMenu = document.getElementById("main-menu");
btnMenu.addEventListener("click", function () {
    if (mainMenu.classList.contains('active')) {
        mainMenu.classList.remove('active');
    } else {
        mainMenu.classList.add('active');
    }
});
window.moveTo = function (id) {
    to = document.getElementById(id);
    to.scrollIntoView({
        block: "start",
        behavior: "smooth"
    });
};
window.sendForm = function (e) {
    e.preventDefault();
    nombre = e.target.nombre.value;
    startup = e.target.startup.value;
    pais = e.target.pais.value;
    web_app = e.target.web_app.value;
    telefono = e.target.telefono.value;
    mail = e.target.mail.value;
    mensaje = e.target.mensaje.value;
    btn_submit = document.getElementById("submit_btn");
    btn_submit.setAttribute("disabled", "true");
    ajax({
        data: {
            nombre: nombre,
            startup: startup,
            pais: pais,
            web_app: web_app,
            telefono: telefono,
            mail: mail,
            mensaje: mensaje
        },
        success: function success() {
            data = JSON.parse(response);
            if (data.status == 'enviado') {
                info = document.getElementById('form-info');
                info.classList.remove("d-none");
                setTimeout(function () {
                    info.classList.add("d-none");
                }, 5000);
            } else {
                info = document.getElementById('form-danger');
                info.classList.remove("d-none");
                setTimeout(function () {
                    info.classList.add("d-none");
                }, 5000);
            }
            btn_submit.removeAttribute("disabled");
        }
    });
};

/***/ }),
/* 2 */
/***/ (function(module, exports) {

window.ajax = function (data) {
	xhr = getAjax();
	xhr.open('POST', '/sendMail', true);
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4) {
			response = xhr.responseText;
			data.success(response);
		}
	};
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("data=" + JSON.stringify(data.data));
};

function getAjax() {
	xmlhttp = false;
	try {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("MsXML2.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
		xmlhttp = new XMLHttpRequest();
	}

	return xmlhttp;
}

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);